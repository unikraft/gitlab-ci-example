## Kraft in GitLab CI

This repository holds a simple workflow that ensures that Unikraft and KraftKit properly work inside the GitLab CI/CD system.
All steps and details are described inside [the unikraft docs](https://github.com/unikraft/docs/tree/main/content/docs/getting-started/integrations/gitlab.mdx).
